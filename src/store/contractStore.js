import { extendObservable, computed } from 'mobx';

class ContractStore {

    constructor() {
        extendObservable(this, {
            number: 1,
            date: new Date().toISOString(),
            success: false,
            loadingBtn: false,
            checked: 'off',
            continueDate: '',
            readOnly: computed(() => { return !this.continueDate })
        });
    }

    changeValue(params) {
        this[params.type] = params.value;
    }

    saveRequestStarted() {
        this.loadingBtn = true;
    }

    saveRequestFinished() {
        this.loadingBtn = false;
    }

    saveRequest() {
        const { saveRequestFinished } = this;
        const that = this;

        this.saveRequestStarted();

        let promise = new Promise((resolve, reject) => {
                setTimeout(function() { resolve( 'ok') }.bind(that), 1000);
            });

        promise.then(({ payload }) => { 
            this.saveRequestFinished();
            this.success = true;
        })
        .catch(({ errors  }) => { console.log(errors) });
    }
}

export default new ContractStore();