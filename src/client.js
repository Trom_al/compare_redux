import React      from 'react';
import ReactDOM   from 'react-dom';
import routes from './routes';
import { browserHistory, Router } from 'react-router';
import contractStore from './store/contractStore';
import { Provider } from 'mobx-react';

const stores = { contractStore };

const component = (
    <Provider {...stores}>
        <Router history={browserHistory}>
            {routes}
        </Router>
    </Provider>
);

ReactDOM.render(component, document.getElementById('react-view'));