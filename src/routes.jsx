import React from 'react';
import { IndexRoute, Route }  from 'react-router';
import App from './components/App';
import AddContractPage from './components/AddContractPage';

export default (
    <Route component={App} path='/'>
        <IndexRoute component={AddContractPage} />
    </Route>
)