import React, { PropTypes, Component } from 'react';
import { FormGroup, ControlLabel, FormControl, Checkbox, Panel } from 'react-bootstrap';
import DatePicker from 'react-bootstrap-date-picker';
import Button from 'react-bootstrap-button-loader';
import { observer, inject } from 'mobx-react';

import './AddContractPage.css';

class AddContractPage extends Component {
  constructor(props) {
    super(props);
    this.handleChangeField = this.handleChangeField.bind(this);
    this.handleChangeDateField = this.handleChangeDateField.bind(this);
    this.handleChangeContinueDateField = this.handleChangeContinueDateField.bind(this);
    this.handleClick = this.handleClick.bind(this)
  }

  handleChangeField(e) {
    const target = e.target;
    this.props.contractStore.changeValue({ type: e.target.name, value: e.target.value });
  }

  handleChangeDateField(value) {
    this.props.contractStore.changeValue({ type: 'date', value: value});
  }

   handleChangeContinueDateField(value, a) {
     this.props.contractStore.changeValue({ type: 'continueDate', value: value});
  }

  handleClick() {
    this.props.contractStore.saveRequest();
  }

  render() {
    const store = this.props.contractStore;
    return (
      <div>
      <h2>Создание договора</h2>
      <h3>Договор №{store.number}</h3>
        <form>
          <FormGroup>
            <ControlLabel>Номер</ControlLabel>
            <FormControl type="text" name="number" value={store.number} onChange={this.handleChangeField}/>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Дата</ControlLabel>
            <DatePicker value={store.date} onChange={this.handleChangeDateField}/>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Дата</ControlLabel>
            <DatePicker value={store.continueDate} onChange={this.handleChangeContinueDateField}/>
          </FormGroup>
          <Checkbox name="checked" value={store.checked} disabled={store.readOnly} onChange={this.handleChangeField}>
            Напомнить об окончании
          </Checkbox>
          <Button loading={store.loadingBtn} onClick={this.handleClick}>Сохранить</Button>
        </form>

        {store.success && <Panel bsStyle="success" className="successMessage">Ура, сохранили</Panel>}
      </div>
    );
  }
}


export default inject('contractStore')(observer(AddContractPage));